<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');

        // $this->middleware('subscribed');

        // $this->middleware('verified');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function home()
    {
        return view('home');
    }

    public function songs()
    {
        return view('songs');
    }

    public function live($id = null)
    {        
        return view('live', array(
            'session_id' => $id
        ));
    }
}

<?php

namespace App\Utils;

class MusicUtils
{
    const EU_REGEXP = '(do|re|mi|fa|sol|la|si)';
    const USA_REGEXP = '[A-G]';
    const F_SHARP_REGEXP = '(b|#)?';
    const CHORD_REGEXP = '(maj|min|m|M|\+|-|dim|aug)?[0-9]*(sus)?[0-9]*';    
    //const CHORD_REGEXP = '/\\b((do|re|mi|fa|sol|la|si)|[A-G])(b|#)?(maj|min|m|M|\+|-|dim|aug)?[0-9]*(sus)?[0-9]*(\/((do|re|mi|fa|sol|la|si)|[A-G])(b|#)?)?\\b/i';

    const EU_2_USA_NOTATION = [
        'DO' => 'C',
        'RE' => 'D',
        'MI' => 'E',
        'FA' => 'F',
        'SOL' => 'G',
        'LA' => 'A',
        'SI' => 'B',        
    ];

    const USA_2_EU_NOTATION = [
        'C' => 'DO',
        'D' => 'RE',
        'E' => 'MI',
        'F' => 'FA',
        'G' => 'SOL',
        'A' => 'LA',
        'B' => 'SI'
    ];

    static function getChordRegexp(){
        //const CHORD_REGEXP = '/\\b((do|re|mi|fa|sol|la|si)|[A-G])(b|#)?(maj|min|m|M|\+|-|dim|aug)?[0-9]*(sus)?[0-9]*(\/((do|re|mi|fa|sol|la|si)|[A-G])(b|#)?)?\\b/i';
        return '/\\b(' . static::EU_REGEXP . '|' . static::USA_REGEXP.')' . static::F_SHARP_REGEXP . static::CHORD_REGEXP.'(\/('.static::EU_REGEXP.'|'.static::USA_REGEXP.')'.static::F_SHARP_REGEXP.')?\\b/i';
    }

    static function convertToUsa($chord)
    {
        if (static::isEuNotation($chord))
        {
            return static::EU_2_USA_NOTATION[strtoupper(static::extractEuNote($chord))];
        }
        else
        {
            return strtoupper($chord[0]);
        }
    }

    private function isEuNotation($note){
        return preg_match(static::EU_REGEXP, $note);
    }

    private function extractEuNote($note){
        preg_match(static::EU_REGEXP, $note, $matches);
        return $matches;
    }

    static public function isChord($value){
        return preg_match(static::getChordRegexp(), $value);
    }

    static public function isChordsLine($raw_value)
    {        
        $chunks = explode(' ', static::clean($raw_value));
        
        $chords = 0;
        $words = 0;

        foreach($chunks as $chunk)
        {
            if (static::isChord($chunk))
                $chords++;
            else
                $words++;
        }

        if ($words > $chords)
            return false;
        else
            return true;
    }

    static public function clean($value)
    {
        // Replace multiple spaces with one space:
        $value = preg_replace('/\s+/', ' ', $value);

        // Replace one or multiple tabs with one space:
        $value = preg_replace('/\t+/', ' ', $value);

        return trim($value);
    }
}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Song extends Model
{
    use Searchable;
    //
    protected $team_id;
    protected $title;    
    protected $author;
    protected $tone;
    protected $lyrics;
    protected $text;

    public function searchableAs()
    {
        return (app()->environment('local') ? '_dev_':'').'cw_songs';
    }
}

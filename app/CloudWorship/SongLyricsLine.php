<?php

namespace App\CloudWorship;

use App\Utils\MusicUtils;

class SongLyricsLine
{
    const TYPE_CHORDS = 'chords';
    const TYPE_WORDS = 'words';    

    public $raw_value;
    public $type;

    public function __construct($value)
    {
        $this->raw_value = $value;
    }

    /* public function parse()
    {
        $this->detectType();
    }

    private function detectType()
    {
        $this->type = $this->isChordsLine() ? static::TYPE_CHORDS : static::TYPE_WORDS;
    } */

    private function clean($value)
    {
        // Replace multiple spaces with one space:
        $value = preg_replace('/\s+/', ' ', $value);

        // Replace one or multiple tabs with one space:
        $value = preg_replace('/\t+/', ' ', $value);

        return trim($value);
    }

    public function isTextLine(){
        return !$this->isChordsLine();
    }

    public function isChordsLine()
    {
        $chunks = explode(' ', $this->clean($this->raw_value));
        
        $chords = 0;
        $words = 0;

        foreach($chunks as $chunk)
        {
            if (MusicUtils::isChord($chunk))
                $chords++;
            else
                $words++;
        }

        if ($words > $chords)
            return false;
        else
            return true;
    }

    private function getChords()
    {
        $chunks = explode(' ', $this->clean($this->raw_value));

        $chords = [];

        foreach($chunks as $chunk)
        {
            if (MusicUtils::isChord($chunk))
                $chords[] = $chunk;
        }

        return $chords;
    }

    public function isEmpty()
    {
        return empty($this->clean($this->raw_value));
    }

    
}
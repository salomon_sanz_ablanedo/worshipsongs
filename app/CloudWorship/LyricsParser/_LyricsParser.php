<?php 

namespace App\CloudWorship\LyricsParser;

abstract class _LyricsParser
{
    public function __construct($url)
    {

    }

    abstract public function parse();
}

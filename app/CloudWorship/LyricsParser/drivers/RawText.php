<?php 

namespace App\CloudWorship\LyricsParser\drivers;

use App\CloudWorship\LyricsParser\_LyricsParser;
use App\CloudWorship\SongLyrics;
use App\CloudWorship\SongLyricsLine;
use App\CloudWorship\SongLyricsGroup;


class RawText extends _LyricsParser
{
    protected $lyrics = null;

    public function __construct($params)
    {
        $this->lyrics = $params['lyrics'];
    }

    public function getText()
    {
        $output = [];
       
        $raw_lines = preg_split('/(\r\n|\n|\r)/', $this->lyrics);
   
        $song = new SongLyrics();

        $prev = null;

        foreach($raw_lines as $l)
        {
            $line = new SongLyricsLine($l);

            if ($line->isEmpty())
            {
                if (!$prev)
                    $output[] = null;
            }
            else if ($line->isTextLine())
                $output[] = trim($line->raw_value);

            $prev = $line->isEmpty();
        }

        $result = implode(PHP_EOL, $output);
        return trim($result);
    }

    public function parse()
    {
        return $this->getLyrics();
    }

    public function getLyrics()
    {
        $output = [];
       
        $raw_lines = preg_split('/(\r\n|\n|\r)/', $this->lyrics);
   
        $song = new SongLyrics();

        $prev = null;

        //$tone = null;

        foreach($raw_lines as $l)
        {
            $line = new SongLyricsLine($l);

            if ($line->isEmpty())
            {
                if (!$prev)
                    $output[] = null;
            }
            else
            {
                $output[] = trim($line->raw_value);
                //if (empty($tone))
                //    $tone = trim($line->getChords()[0]);
            }

            $prev = $line->isEmpty();
        }

        $result = implode(PHP_EOL, $output);
        return trim($result);
    }

    //public function 
}
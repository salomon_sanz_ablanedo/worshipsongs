@extends('spark::layouts.app')

@section('content')

<songs :user="user" inline-template>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card card-default">
                    <div class="card-body">
                        <div class="form form-inline d-flex">
                            <input type="text" name="" class="form-control flex-grow-1" :placeholder="'Buscar en '+ totalSongs + ' canciones'">
                            <button type="button" class="btn btn-default mx-1"><i class="fa fa-plus-square"></i> Nuevo</button>
                            <button type="button" class="btn btn-default" @click="_showImportModal"><i class="fa fa-cloud-download"></i> Importar</button>
                        </div>
                    </div>

                    <div class="card-header">
                        <div v-for="song in songs">
                        @{{ song.title }}
                        <hr/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    
</songs>



@endsection

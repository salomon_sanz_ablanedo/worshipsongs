//window._ = require('lodash');

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */
/*
window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
axios.defaults.withCredentials = true;
 */
/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

import Echo from 'laravel-echo';

window.Pusher = require('pusher-js');

window.Echo = new Echo({
    broadcaster: 'pusher',
    key: '32a27a50203411538fe2',//process.env.MIX_PUSHER_APP_KEY,
    cluster: 'eu',//process.env.MIX_PUSHER_APP_CLUSTER,
    //forceTLS: false,
    encrypted : false
});

require('./spark-components/bootstrap');
require('./components/home');
require('./components/songs');

import InstantSearch from 'vue-instantsearch';

Vue.use(InstantSearch);

// autoregister all the components inside cart folder
const files = require.context('./components/vue/', true, /\.vue$/i);
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));


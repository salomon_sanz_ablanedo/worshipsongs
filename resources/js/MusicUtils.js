const EU_REGEXP = '(do|re|mi|fa|sol|la|si)';
const USA_REGEXP = '[A-G]';
const F_SHARP_REGEXP = '(b|#)?';
const CHORD_REGEXP = '(maj|min|m|M|\\+|-|dim|aug)?[0-9]*(sus)?[0-9]*';    
//const CHORD_REGEXP = '/\\b((do|re|mi|fa|sol|la|si)|[A-G])(b|#)?(maj|min|m|M|\+|-|dim|aug)?[0-9]*(sus)?[0-9]*(\/((do|re|mi|fa|sol|la|si)|[A-G])(b|#)?)?\\b/i';

const EU_2_USA_NOTATION = {
    DO : 'C',
    RE : 'D',
    MI : 'E',
    FA : 'F',
    SOL : 'G',
    LA : 'A',
    SI : 'B',        
};

const USA_2_EU_NOTATION = {
    C : 'DO',
    D : 'RE',
    E : 'MI',
    F : 'FA',
    G : 'SOL',
    A : 'LA',
    B : 'SI'
};
//return '/\\b(' EU_REGEXP . '|' USA_REGEXP.')' F_SHARP_REGEXP CHORD_REGEXP.'(\/('.static::EU_REGEXP.'|'.static::USA_REGEXP.')'.static::F_SHARP_REGEXP.')?\\b/i';
const CHORD_REGEXP_DETECTOR = `\\b(${EU_REGEXP}|${USA_REGEXP})${F_SHARP_REGEXP}${CHORD_REGEXP}(\/(${EU_REGEXP}|${USA_REGEXP})${F_SHARP_REGEXP})?\\b/i`;

const MusicUtils = {    

    isChord(text){
        var exp = `\\b(${EU_REGEXP}|${USA_REGEXP})${F_SHARP_REGEXP}${CHORD_REGEXP}(\/(${EU_REGEXP}|${USA_REGEXP})${F_SHARP_REGEXP})?\\b`;
        var re = new RegExp(exp, "i");
        let result = text.match(re);        
        return result;
    }

}

export default MusicUtils;
import axios from 'axios';

const type = 'SongLyrics';
const name = 'Canción'

class SongLyrics
{        

    constructor(item = null){  

        this.id = null;
        this.title = null;        

        this.tone = null;                
        this.author = null;
        this.first_phrase = null;
        this.text = null;
        this.lyrics = null;
        this.type  = type;
        this.created_at = null;
        this.updated_at = null;    

        if (!item) return;

        if (typeof item == 'object')
        {
            for (let key in item)
            {
                if (key in this)
                    this[key] = item[key];
            }
        }
        else if (typeof item == 'number')
            this.id = item;

        //super(item);
    }

    getFieldsForQueueInfo(){
        return ['id','title','tone'];
    }

    static getName(){
        return name;
    }

    getName(){
        return name;
    }

    static getIcon(){
        return 'fa fa-music';
    }

    static get type(){
        return type;
    }
    
    getQueueTitle(){
        return this.title;
    }

    getQueueBadge(){
        return this.tone;
    }

    getComponentViewName(){        
        return `${type}View`
    }

    getComponentFormName(){        
        return `${type}Form`
    }

    getTypeConfigStorageKey(){
        return `${type}-conf`
    }

    getResourceStorageKey(){
        return `loadable-${this.id}`;
    }

    store()
    {                
        const url = '/api/songlyrics' + (this.id ? `/${this.id}`:'');

        return axios.post(url, {
            title : this.title,
            author : this.author,
            tone : this.tone,
            lyrics : this.lyrics
        });
    }

    delete()
    {                
        const url = '/api/songlyrics/' + this.id;
        return axios.delete(url);
    }

    static find(id)
    {        
        const url = `/api/${type.toLowerCase()}/${id}`;

        return fetch(url)
            .then(response => response.json())  
            .then(r=>{                
            return new this(r.data);                
        });
    }
    
    static getDefaultConfig(){

        return {
            defaultChordsStyle : {
                'font-size' : '14px',
                'color' : '#999',        
            },            
            defaultLyricsStyle : {
                'color' : '#000',
                'font-size' : '24px',        
            },
            defaultSeparatorStyle : {
                'height' : '32px'
            }
        }
    }   

    getConfigurableModel(){
        return {
            showChords      : null,
            chordsColor     : null,
            chordsSize      : null,
            lyricsColor     : null,
            lyricsSize      : null,
            separatorHeight : null
        }
    }

    getConfigurableSchema(){
        return {
            type: 'object',
            properties: {
                showChords: { 
                    title   : 'Mostrar acordes',
                    type    : 'boolean',
                    default : true,                    
                    required : false                    
                },
                chordsColor: { 
                    title   : 'Color acordes',
                    type    : 'string',
                    default : '#999',                    
                    required : false,
                    uiType  : "color"
                },
                chordsSize:{
                    title   : 'Tamaño acordes',
                    type    : 'number',
                    default : 14,
                    minimum : 10,
                    maximum : 32,
                    required : false,
                    uiType  : "range"
                },
                lyricsColor :{
                    title   : 'Color letra',
                    type    : 'string',
                    default : '#000',                    
                    required : false,
                    uiType  : "color"
                },
                lyricsSize : {
                    title   : 'Tamaño letra',
                    type    : 'number',
                    default : 24,
                    minimum : 10,
                    maximum : 32,
                    required : false,
                    uiType  : "range"
                },
                separatorHeight : {
                    title   : 'Separación',
                    type    : 'number',
                    default : 32,
                    minimum : 0,
                    maximum : 64,
                    required : false,
                    uiType  : "range"
                }                
            }
        }
    }

}

export default SongLyrics;
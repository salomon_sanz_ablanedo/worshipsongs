Vue.component('home', {
    props: ['user'],

    data(){
        return {
            total_songs : 0
        }
    },

    mounted() {
        this.loadTotalSongs()
    },

    methods : {
        loadTotalSongs(){
            this.total_songs = Math.random();
        }
    }
});

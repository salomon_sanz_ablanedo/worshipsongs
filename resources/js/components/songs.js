/* 
Vue.component(
    'rawtext-importer',
    require('./vue/loadableForms/songlyrics.vue').default
); 
*/

Vue.component('songs', {
    
    props: ['user'],

    data(){
        return {
            totalSongs : 0,
            songs : [],
            showModalImport : false,
            importDrivers : [],
            selectedImportDriver : null,
            import_step : 1
        }
    },

    created(){
        this.loadSongs();
        this.loadTotalSongs();
        this.loadImportDrivers();
    },

    methods : {
        loadTotalSongs(){
            this.totalSongs = Math.random();
        },

        loadSongs(){
            this.songs = [
                {title : 'Venciendo en fe'},
                {title : 'Fidelidad'}
            ];
        },

        loadImportDrivers()
        {
            this.importDrivers = [
                {
                    name : 'Texto plano',
                    slug : 'raw_text',
                    importFormComponent : 'rawtext-importer'
                },
                {
                    name : 'Google Docs',
                    slug : 'google_docs',
                    importFormComponent : 'googledocs-importer'
                },
                {
                    name : 'LaCuerda.net',
                    slug : 'lacuerda_net',
                    importFormComponent : 'lacuerdanet-importer'
                }
            ];
        },

        _showImportModal()
        {
            this.showModalImport = true;
        },

        _startImport(){
            this.$refs.importFormComponent.import();
            this.import_step = 3;
        }
    }
});



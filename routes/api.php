<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register the API routes for your application as
| the routes are automatically authenticated using the API guard and
| loaded automatically by this application's RouteServiceProvider.
|
*/
use App\Http\Resources\Song as SongResource;
use App\Models\Song;
use Illuminate\Http\Request;

Route::group([
    //'middleware' => 'auth:api'
], function () {
    
    Route::get('songlyrics/{id}', function($id){
        return new SongResource(Song::findOrFail($id));
    });

    Route::post('songlyrics/{id?}', function(        
        \App\Models\Song $Song,
        Request $request,
        $id = null
    )
    {
        $class = '\\App\\CloudWorship\\LyricsParser\\drivers\\RawText';

        $importer = new $class($request->all());

        $song = empty($id)? new Song : Song::find($id);
        $song->title = $request->get('title');
        $song->author = $request->get('author', null);
        $song->tone = $request->get('tone', null);
        $song->lyrics = $importer->getLyrics();
        $song->text = $importer->getText();
        $song->save();        

        return new SongResource($song);
    });

    Route::delete('songlyrics/{id}', function(
        \App\Models\Song $Song,
        Request $request,
        $id = null
    ){        
        $song = Song::find($id);
        $song->delete();

        return response()->json([
            'result' => 'ok'            
        ]);
    });
});
